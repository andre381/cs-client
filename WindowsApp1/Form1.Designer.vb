﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.dglap = New System.Windows.Forms.DataGridView()
        Me.petugas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.setengah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tuju = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigasatu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.delapan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.empat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sembilan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tiga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.enam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dua = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lima = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rata = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sepuluh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sebelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.duabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dt6 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.dt = New System.Windows.Forms.DateTimePicker()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.dg2 = New System.Windows.Forms.DataGridView()
        Me.cs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.agen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pesan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.antusias = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cuek = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DetailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailCuekToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dt5 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.dt2 = New System.Windows.Forms.DateTimePicker()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.dgtab3 = New System.Windows.Forms.DataGridView()
        Me.cst3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kurang30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.present3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigapuluhsatu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.presentdua = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satumenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.presentiga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigamenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.limamenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.limabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigapuluh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lebih = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ratat3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsampe45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persenempat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsampesatu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persenlima = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtt4 = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.dtt3 = New System.Windows.Forms.DateTimePicker()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.dg3 = New System.Windows.Forms.DataGridView()
        Me.csid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kodeagen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgWaktu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dt4 = New System.Windows.Forms.DateTimePicker()
        Me.aveall = New System.Windows.Forms.Label()
        Me.cmbcs2 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.dt3 = New System.Windows.Forms.DateTimePicker()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.warna3 = New System.Windows.Forms.TextBox()
        Me.warna2 = New System.Windows.Forms.TextBox()
        Me.warna1 = New System.Windows.Forms.TextBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dglap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.dg2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.Panel20.SuspendLayout()
        CType(Me.dgtab3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel15.SuspendLayout()
        CType(Me.dg3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(890, 432)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel5)
        Me.TabPage1.Controls.Add(Me.Panel4)
        Me.TabPage1.Controls.Add(Me.Panel3)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(882, 406)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Waktu Salam Awal"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.dglap)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(18, 47)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(846, 337)
        Me.Panel5.TabIndex = 9
        '
        'dglap
        '
        Me.dglap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dglap.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.petugas, Me.setengah, Me.tuju, Me.tigasatu, Me.delapan, Me.empat, Me.sembilan, Me.tiga, Me.enam, Me.dua, Me.satu, Me.lima, Me.rata, Me.sepuluh, Me.sebelas, Me.duabelas, Me.tigabelas})
        Me.dglap.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dglap.Location = New System.Drawing.Point(0, 0)
        Me.dglap.Name = "dglap"
        Me.dglap.Size = New System.Drawing.Size(846, 337)
        Me.dglap.TabIndex = 3
        '
        'petugas
        '
        Me.petugas.HeaderText = "Petugas"
        Me.petugas.Name = "petugas"
        '
        'setengah
        '
        Me.setengah.HeaderText = "≤30 detik"
        Me.setengah.Name = "setengah"
        '
        'tuju
        '
        Me.tuju.HeaderText = "%"
        Me.tuju.Name = "tuju"
        '
        'tigasatu
        '
        Me.tigasatu.HeaderText = ">30 Detik & ≤45 Detik"
        Me.tigasatu.Name = "tigasatu"
        '
        'delapan
        '
        Me.delapan.HeaderText = "%"
        Me.delapan.Name = "delapan"
        '
        'empat
        '
        Me.empat.HeaderText = "> 45 Detik & ≤1 Menit"
        Me.empat.Name = "empat"
        '
        'sembilan
        '
        Me.sembilan.HeaderText = "%"
        Me.sembilan.Name = "sembilan"
        '
        'tiga
        '
        Me.tiga.HeaderText = ">1 & ≤3 Menit"
        Me.tiga.Name = "tiga"
        '
        'enam
        '
        Me.enam.HeaderText = ">3 & ≤5 menit"
        Me.enam.Name = "enam"
        '
        'dua
        '
        Me.dua.HeaderText = ">5 & ≤15 Menit"
        Me.dua.Name = "dua"
        '
        'satu
        '
        Me.satu.HeaderText = ">15 & ≤30 Menit"
        Me.satu.Name = "satu"
        '
        'lima
        '
        Me.lima.HeaderText = "> 30 Menit"
        Me.lima.Name = "lima"
        '
        'rata
        '
        Me.rata.HeaderText = "Jumlah Chat"
        Me.rata.Name = "rata"
        '
        'sepuluh
        '
        Me.sepuluh.HeaderText = "0 - 45 detik"
        Me.sepuluh.Name = "sepuluh"
        '
        'sebelas
        '
        Me.sebelas.HeaderText = "%"
        Me.sebelas.Name = "sebelas"
        '
        'duabelas
        '
        Me.duabelas.HeaderText = "0 - 1 Menit"
        Me.duabelas.Name = "duabelas"
        '
        'tigabelas
        '
        Me.tigabelas.HeaderText = "%"
        Me.tigabelas.Name = "tigabelas"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.dt6)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.Button1)
        Me.Panel4.Controls.Add(Me.dt)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(18, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(846, 44)
        Me.Panel4.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(172, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(18, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "sd"
        '
        'dt6
        '
        Me.dt6.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt6.Location = New System.Drawing.Point(196, 10)
        Me.dt6.Name = "dt6"
        Me.dt6.Size = New System.Drawing.Size(108, 20)
        Me.dt6.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tanggal"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(310, 9)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Proses"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'dt
        '
        Me.dt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt.Location = New System.Drawing.Point(58, 10)
        Me.dt.Name = "dt"
        Me.dt.Size = New System.Drawing.Size(108, 20)
        Me.dt.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(18, 384)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(846, 19)
        Me.Panel3.TabIndex = 7
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(15, 400)
        Me.Panel2.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel1.Location = New System.Drawing.Point(864, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(15, 400)
        Me.Panel1.TabIndex = 5
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel10)
        Me.TabPage2.Controls.Add(Me.Panel6)
        Me.TabPage2.Controls.Add(Me.Panel7)
        Me.TabPage2.Controls.Add(Me.Panel8)
        Me.TabPage2.Controls.Add(Me.Panel9)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(882, 406)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Antusias Balasan"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.dg2)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(18, 47)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(846, 337)
        Me.Panel10.TabIndex = 14
        '
        'dg2
        '
        Me.dg2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cs, Me.agen, Me.pesan, Me.tanggal, Me.antusias, Me.cuek, Me.persen})
        Me.dg2.ContextMenuStrip = Me.ContextMenuStrip1
        Me.dg2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg2.Location = New System.Drawing.Point(0, 0)
        Me.dg2.Name = "dg2"
        Me.dg2.Size = New System.Drawing.Size(846, 337)
        Me.dg2.TabIndex = 10
        '
        'cs
        '
        Me.cs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cs.HeaderText = "Petugas"
        Me.cs.Name = "cs"
        '
        'agen
        '
        Me.agen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.agen.HeaderText = "Kode Agen"
        Me.agen.Name = "agen"
        Me.agen.Visible = False
        '
        'pesan
        '
        Me.pesan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.pesan.HeaderText = "Pesan"
        Me.pesan.Name = "pesan"
        Me.pesan.Visible = False
        '
        'tanggal
        '
        Me.tanggal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.tanggal.HeaderText = "Tanggal"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.Visible = False
        '
        'antusias
        '
        Me.antusias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.antusias.HeaderText = "Antusias"
        Me.antusias.Name = "antusias"
        '
        'cuek
        '
        Me.cuek.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cuek.HeaderText = "Cuek"
        Me.cuek.Name = "cuek"
        '
        'persen
        '
        Me.persen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.persen.HeaderText = "Persentase Cuek"
        Me.persen.Name = "persen"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetailToolStripMenuItem, Me.DetailCuekToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(151, 48)
        '
        'DetailToolStripMenuItem
        '
        Me.DetailToolStripMenuItem.Name = "DetailToolStripMenuItem"
        Me.DetailToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.DetailToolStripMenuItem.Text = "Detail antusias"
        '
        'DetailCuekToolStripMenuItem
        '
        Me.DetailCuekToolStripMenuItem.Name = "DetailCuekToolStripMenuItem"
        Me.DetailCuekToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.DetailCuekToolStripMenuItem.Text = "Detail Cuek"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.dt5)
        Me.Panel6.Controls.Add(Me.Label2)
        Me.Panel6.Controls.Add(Me.Button2)
        Me.Panel6.Controls.Add(Me.dt2)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(18, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(846, 44)
        Me.Panel6.TabIndex = 13
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(172, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "sd"
        '
        'dt5
        '
        Me.dt5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt5.Location = New System.Drawing.Point(196, 10)
        Me.dt5.Name = "dt5"
        Me.dt5.Size = New System.Drawing.Size(108, 20)
        Me.dt5.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(310, 9)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Proses"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'dt2
        '
        Me.dt2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt2.Location = New System.Drawing.Point(58, 10)
        Me.dt2.Name = "dt2"
        Me.dt2.Size = New System.Drawing.Size(108, 20)
        Me.dt2.TabIndex = 2
        '
        'Panel7
        '
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(864, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(15, 381)
        Me.Panel7.TabIndex = 10
        '
        'Panel8
        '
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel8.Location = New System.Drawing.Point(18, 384)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(861, 19)
        Me.Panel8.TabIndex = 12
        '
        'Panel9
        '
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel9.Location = New System.Drawing.Point(3, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(15, 400)
        Me.Panel9.TabIndex = 11
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel20)
        Me.TabPage4.Controls.Add(Me.Panel16)
        Me.TabPage4.Controls.Add(Me.Panel17)
        Me.TabPage4.Controls.Add(Me.Panel18)
        Me.TabPage4.Controls.Add(Me.Panel19)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(882, 406)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Kecepatan Balasan"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.dgtab3)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel20.Location = New System.Drawing.Point(18, 47)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(846, 337)
        Me.Panel20.TabIndex = 24
        '
        'dgtab3
        '
        Me.dgtab3.AllowUserToAddRows = False
        Me.dgtab3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgtab3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cst3, Me.kurang30, Me.present3, Me.tigapuluhsatu, Me.presentdua, Me.satumenit, Me.presentiga, Me.tigamenit, Me.limamenit, Me.limabelas, Me.tigapuluh, Me.lebih, Me.ratat3, Me.nolsampe45, Me.persenempat, Me.nolsampesatu, Me.persenlima})
        Me.dgtab3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgtab3.Location = New System.Drawing.Point(0, 0)
        Me.dgtab3.Name = "dgtab3"
        Me.dgtab3.Size = New System.Drawing.Size(846, 337)
        Me.dgtab3.TabIndex = 4
        '
        'cst3
        '
        Me.cst3.HeaderText = "Petugas"
        Me.cst3.Name = "cst3"
        '
        'kurang30
        '
        Me.kurang30.HeaderText = "≤30 detik"
        Me.kurang30.Name = "kurang30"
        '
        'present3
        '
        Me.present3.HeaderText = "%"
        Me.present3.Name = "present3"
        '
        'tigapuluhsatu
        '
        Me.tigapuluhsatu.HeaderText = ">30 & ≤45 Detik"
        Me.tigapuluhsatu.Name = "tigapuluhsatu"
        '
        'presentdua
        '
        Me.presentdua.HeaderText = "%"
        Me.presentdua.Name = "presentdua"
        '
        'satumenit
        '
        Me.satumenit.HeaderText = ">45 Detik & ≤1 Menit"
        Me.satumenit.Name = "satumenit"
        '
        'presentiga
        '
        Me.presentiga.HeaderText = "%"
        Me.presentiga.Name = "presentiga"
        '
        'tigamenit
        '
        Me.tigamenit.HeaderText = ">1 & ≤3 Menit"
        Me.tigamenit.Name = "tigamenit"
        '
        'limamenit
        '
        Me.limamenit.HeaderText = ">3 & ≤5 menit"
        Me.limamenit.Name = "limamenit"
        '
        'limabelas
        '
        Me.limabelas.HeaderText = ">5 & ≤15 Menit"
        Me.limabelas.Name = "limabelas"
        '
        'tigapuluh
        '
        Me.tigapuluh.HeaderText = ">15 & ≤30 Menit"
        Me.tigapuluh.Name = "tigapuluh"
        '
        'lebih
        '
        Me.lebih.HeaderText = "> 30 Menit"
        Me.lebih.Name = "lebih"
        '
        'ratat3
        '
        Me.ratat3.HeaderText = "Jumlah Chat"
        Me.ratat3.Name = "ratat3"
        '
        'nolsampe45
        '
        Me.nolsampe45.HeaderText = "0 - 45 detik"
        Me.nolsampe45.Name = "nolsampe45"
        '
        'persenempat
        '
        Me.persenempat.HeaderText = "%"
        Me.persenempat.Name = "persenempat"
        '
        'nolsampesatu
        '
        Me.nolsampesatu.HeaderText = "0 - 1 Menit"
        Me.nolsampesatu.Name = "nolsampesatu"
        '
        'persenlima
        '
        Me.persenlima.HeaderText = "%"
        Me.persenlima.Name = "persenlima"
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.warna1)
        Me.Panel16.Controls.Add(Me.warna2)
        Me.Panel16.Controls.Add(Me.warna3)
        Me.Panel16.Controls.Add(Me.Label7)
        Me.Panel16.Controls.Add(Me.dtt4)
        Me.Panel16.Controls.Add(Me.Label9)
        Me.Panel16.Controls.Add(Me.Button4)
        Me.Panel16.Controls.Add(Me.dtt3)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel16.Location = New System.Drawing.Point(18, 3)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(846, 44)
        Me.Panel16.TabIndex = 23
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(172, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(18, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "sd"
        '
        'dtt4
        '
        Me.dtt4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtt4.Location = New System.Drawing.Point(196, 10)
        Me.dtt4.Name = "dtt4"
        Me.dtt4.Size = New System.Drawing.Size(108, 20)
        Me.dtt4.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(46, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Tanggal"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(310, 9)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 4
        Me.Button4.Text = "Proses"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'dtt3
        '
        Me.dtt3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtt3.Location = New System.Drawing.Point(58, 10)
        Me.dtt3.Name = "dtt3"
        Me.dtt3.Size = New System.Drawing.Size(108, 20)
        Me.dtt3.TabIndex = 2
        '
        'Panel17
        '
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel17.Location = New System.Drawing.Point(864, 3)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(15, 381)
        Me.Panel17.TabIndex = 20
        '
        'Panel18
        '
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel18.Location = New System.Drawing.Point(18, 384)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(861, 19)
        Me.Panel18.TabIndex = 22
        '
        'Panel19
        '
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel19.Location = New System.Drawing.Point(3, 3)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(15, 400)
        Me.Panel19.TabIndex = 21
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel15)
        Me.TabPage3.Controls.Add(Me.Panel11)
        Me.TabPage3.Controls.Add(Me.Panel12)
        Me.TabPage3.Controls.Add(Me.Panel13)
        Me.TabPage3.Controls.Add(Me.Panel14)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(882, 406)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Rata Rata Waktu Balas"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.dg3)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel15.Location = New System.Drawing.Point(18, 47)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(846, 337)
        Me.Panel15.TabIndex = 19
        '
        'dg3
        '
        Me.dg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.csid, Me.kodeagen, Me.ave, Me.dgWaktu})
        Me.dg3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg3.Location = New System.Drawing.Point(0, 0)
        Me.dg3.Name = "dg3"
        Me.dg3.Size = New System.Drawing.Size(846, 337)
        Me.dg3.TabIndex = 11
        '
        'csid
        '
        Me.csid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.csid.HeaderText = "Petugas"
        Me.csid.Name = "csid"
        '
        'kodeagen
        '
        Me.kodeagen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.kodeagen.HeaderText = "Kode Agen"
        Me.kodeagen.Name = "kodeagen"
        '
        'ave
        '
        Me.ave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ave.HeaderText = "Rata Rata"
        Me.ave.Name = "ave"
        '
        'dgWaktu
        '
        Me.dgWaktu.HeaderText = "Column1"
        Me.dgWaktu.Name = "dgWaktu"
        Me.dgWaktu.Visible = False
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.Label4)
        Me.Panel11.Controls.Add(Me.dt4)
        Me.Panel11.Controls.Add(Me.aveall)
        Me.Panel11.Controls.Add(Me.cmbcs2)
        Me.Panel11.Controls.Add(Me.Label3)
        Me.Panel11.Controls.Add(Me.Button3)
        Me.Panel11.Controls.Add(Me.dt3)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(18, 3)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(846, 44)
        Me.Panel11.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(172, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "sd"
        '
        'dt4
        '
        Me.dt4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt4.Location = New System.Drawing.Point(196, 10)
        Me.dt4.Name = "dt4"
        Me.dt4.Size = New System.Drawing.Size(108, 20)
        Me.dt4.TabIndex = 7
        '
        'aveall
        '
        Me.aveall.AutoSize = True
        Me.aveall.Dock = System.Windows.Forms.DockStyle.Right
        Me.aveall.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.aveall.Location = New System.Drawing.Point(775, 0)
        Me.aveall.Name = "aveall"
        Me.aveall.Padding = New System.Windows.Forms.Padding(0, 10, 10, 10)
        Me.aveall.Size = New System.Drawing.Size(71, 40)
        Me.aveall.TabIndex = 6
        Me.aveall.Text = "0 Menit"
        '
        'cmbcs2
        '
        Me.cmbcs2.FormattingEnabled = True
        Me.cmbcs2.Items.AddRange(New Object() {"all"})
        Me.cmbcs2.Location = New System.Drawing.Point(315, 9)
        Me.cmbcs2.Name = "cmbcs2"
        Me.cmbcs2.Size = New System.Drawing.Size(121, 21)
        Me.cmbcs2.TabIndex = 5
        Me.cmbcs2.Text = "all"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tanggal"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(442, 9)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Proses"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'dt3
        '
        Me.dt3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt3.Location = New System.Drawing.Point(58, 10)
        Me.dt3.Name = "dt3"
        Me.dt3.Size = New System.Drawing.Size(108, 20)
        Me.dt3.TabIndex = 2
        '
        'Panel12
        '
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel12.Location = New System.Drawing.Point(864, 3)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(15, 381)
        Me.Panel12.TabIndex = 14
        '
        'Panel13
        '
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel13.Location = New System.Drawing.Point(18, 384)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(861, 19)
        Me.Panel13.TabIndex = 17
        '
        'Panel14
        '
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel14.Location = New System.Drawing.Point(3, 3)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(15, 400)
        Me.Panel14.TabIndex = 16
        '
        'warna3
        '
        Me.warna3.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.warna3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna3.Enabled = False
        Me.warna3.Location = New System.Drawing.Point(775, 14)
        Me.warna3.Name = "warna3"
        Me.warna3.ReadOnly = True
        Me.warna3.Size = New System.Drawing.Size(65, 20)
        Me.warna3.TabIndex = 19
        Me.warna3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'warna2
        '
        Me.warna2.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(19, Byte), Integer))
        Me.warna2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna2.Enabled = False
        Me.warna2.Location = New System.Drawing.Point(704, 14)
        Me.warna2.Name = "warna2"
        Me.warna2.ReadOnly = True
        Me.warna2.Size = New System.Drawing.Size(65, 20)
        Me.warna2.TabIndex = 20
        Me.warna2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'warna1
        '
        Me.warna1.BackColor = System.Drawing.Color.Lime
        Me.warna1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna1.Enabled = False
        Me.warna1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna1.Location = New System.Drawing.Point(633, 14)
        Me.warna1.Name = "warna1"
        Me.warna1.ReadOnly = True
        Me.warna1.Size = New System.Drawing.Size(65, 20)
        Me.warna1.TabIndex = 21
        Me.warna1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(890, 432)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Performa Cs"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.dglap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        CType(Me.dg2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.Panel20.ResumeLayout(False)
        CType(Me.dgtab3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel15.ResumeLayout(False)
        CType(Me.dg3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Panel5 As Panel
    Friend WithEvents dglap As DataGridView
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents dt As DateTimePicker
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents dt2 As DateTimePicker
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents dg2 As DataGridView
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Panel11 As Panel
    Friend WithEvents cmbcs2 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents dt3 As DateTimePicker
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Panel14 As Panel
    Friend WithEvents dg3 As DataGridView
    Friend WithEvents aveall As Label
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents DetailToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DetailCuekToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents cs As DataGridViewTextBoxColumn
    Friend WithEvents agen As DataGridViewTextBoxColumn
    Friend WithEvents pesan As DataGridViewTextBoxColumn
    Friend WithEvents tanggal As DataGridViewTextBoxColumn
    Friend WithEvents antusias As DataGridViewTextBoxColumn
    Friend WithEvents cuek As DataGridViewTextBoxColumn
    Friend WithEvents persen As DataGridViewTextBoxColumn
    Friend WithEvents Label4 As Label
    Friend WithEvents dt4 As DateTimePicker
    Friend WithEvents csid As DataGridViewTextBoxColumn
    Friend WithEvents kodeagen As DataGridViewTextBoxColumn
    Friend WithEvents ave As DataGridViewTextBoxColumn
    Friend WithEvents dgWaktu As DataGridViewTextBoxColumn
    Friend WithEvents Label5 As Label
    Friend WithEvents dt5 As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents dt6 As DateTimePicker
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Panel20 As Panel
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents dtt4 As DateTimePicker
    Friend WithEvents Label9 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents dtt3 As DateTimePicker
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Panel19 As Panel
    Friend WithEvents dgtab3 As DataGridView
    Friend WithEvents cst3 As DataGridViewTextBoxColumn
    Friend WithEvents kurang30 As DataGridViewTextBoxColumn
    Friend WithEvents present3 As DataGridViewTextBoxColumn
    Friend WithEvents tigapuluhsatu As DataGridViewTextBoxColumn
    Friend WithEvents presentdua As DataGridViewTextBoxColumn
    Friend WithEvents satumenit As DataGridViewTextBoxColumn
    Friend WithEvents presentiga As DataGridViewTextBoxColumn
    Friend WithEvents tigamenit As DataGridViewTextBoxColumn
    Friend WithEvents limamenit As DataGridViewTextBoxColumn
    Friend WithEvents limabelas As DataGridViewTextBoxColumn
    Friend WithEvents tigapuluh As DataGridViewTextBoxColumn
    Friend WithEvents lebih As DataGridViewTextBoxColumn
    Friend WithEvents ratat3 As DataGridViewTextBoxColumn
    Friend WithEvents nolsampe45 As DataGridViewTextBoxColumn
    Friend WithEvents persenempat As DataGridViewTextBoxColumn
    Friend WithEvents nolsampesatu As DataGridViewTextBoxColumn
    Friend WithEvents persenlima As DataGridViewTextBoxColumn
    Friend WithEvents petugas As DataGridViewTextBoxColumn
    Friend WithEvents setengah As DataGridViewTextBoxColumn
    Friend WithEvents tuju As DataGridViewTextBoxColumn
    Friend WithEvents tigasatu As DataGridViewTextBoxColumn
    Friend WithEvents delapan As DataGridViewTextBoxColumn
    Friend WithEvents empat As DataGridViewTextBoxColumn
    Friend WithEvents sembilan As DataGridViewTextBoxColumn
    Friend WithEvents tiga As DataGridViewTextBoxColumn
    Friend WithEvents enam As DataGridViewTextBoxColumn
    Friend WithEvents dua As DataGridViewTextBoxColumn
    Friend WithEvents satu As DataGridViewTextBoxColumn
    Friend WithEvents lima As DataGridViewTextBoxColumn
    Friend WithEvents rata As DataGridViewTextBoxColumn
    Friend WithEvents sepuluh As DataGridViewTextBoxColumn
    Friend WithEvents sebelas As DataGridViewTextBoxColumn
    Friend WithEvents duabelas As DataGridViewTextBoxColumn
    Friend WithEvents tigabelas As DataGridViewTextBoxColumn
    Friend WithEvents warna1 As TextBox
    Friend WithEvents warna2 As TextBox
    Friend WithEvents warna3 As TextBox
End Class
