﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.dglap = New System.Windows.Forms.DataGridView()
        Me.petugas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.setengah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tuju = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sepuluh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sebelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsatumenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.noolsatupersen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.duabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tiga = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satulimapersen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigasatu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.delapan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.empat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sembilan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.enam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dua = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.satu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lima = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rata = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.cmblevel1 = New System.Windows.Forms.ComboBox()
        Me.Panel28 = New System.Windows.Forms.Panel()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Panel29 = New System.Windows.Forms.Panel()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Panel30 = New System.Windows.Forms.Panel()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Panel31 = New System.Windows.Forms.Panel()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Panel32 = New System.Windows.Forms.Panel()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Panel33 = New System.Windows.Forms.Panel()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Panel34 = New System.Windows.Forms.Panel()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.dt6 = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dt = New System.Windows.Forms.DateTimePicker()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.dgtab3 = New System.Windows.Forms.DataGridView()
        Me.cst3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kurang30 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.present3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsampe45 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persenempat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsampesatumenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsatupersen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nolsampesatu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persenlima = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.limamenit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persent4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.limabelas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persent5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tigapuluh = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persent6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lebih = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persent7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ratat3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.cmblevel2 = New System.Windows.Forms.ComboBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Panel23 = New System.Windows.Forms.Panel()
        Me.warna1 = New System.Windows.Forms.TextBox()
        Me.Panel22 = New System.Windows.Forms.Panel()
        Me.warna2 = New System.Windows.Forms.TextBox()
        Me.Panel24 = New System.Windows.Forms.Panel()
        Me.warna4 = New System.Windows.Forms.TextBox()
        Me.Panel21 = New System.Windows.Forms.Panel()
        Me.warna3 = New System.Windows.Forms.TextBox()
        Me.Panel25 = New System.Windows.Forms.Panel()
        Me.warna5 = New System.Windows.Forms.TextBox()
        Me.Panel26 = New System.Windows.Forms.Panel()
        Me.warna6 = New System.Windows.Forms.TextBox()
        Me.Panel27 = New System.Windows.Forms.Panel()
        Me.warna7 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dtt4 = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dtt3 = New System.Windows.Forms.DateTimePicker()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.dg2 = New System.Windows.Forms.DataGridView()
        Me.cs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.agen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pesan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tanggal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.antusias = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cuek = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.persen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.strip_antusias = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DetailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DetailCuekToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.cmblevel3 = New System.Windows.Forms.ComboBox()
        Me.Panel43 = New System.Windows.Forms.Panel()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dt5 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.dt2 = New System.Windows.Forms.DateTimePicker()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.dgrate = New System.Windows.Forms.DataGridView()
        Me.ratecs = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.rate5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jmlrate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel35 = New System.Windows.Forms.Panel()
        Me.cmblevel4 = New System.Windows.Forms.ComboBox()
        Me.Panel44 = New System.Windows.Forms.Panel()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.dtrate2 = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtrate1 = New System.Windows.Forms.DateTimePicker()
        Me.Panel38 = New System.Windows.Forms.Panel()
        Me.Panel37 = New System.Windows.Forms.Panel()
        Me.Panel36 = New System.Windows.Forms.Panel()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.dgistirahat = New System.Windows.Forms.DataGridView()
        Me.cspetugas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.offistirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jml1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.waktuistirahat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jml2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.break = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jml3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ibadah = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.jml4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel42 = New System.Windows.Forms.Panel()
        Me.Panel41 = New System.Windows.Forms.Panel()
        Me.cmblevel5 = New System.Windows.Forms.ComboBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtisti2 = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtisti1 = New System.Windows.Forms.DateTimePicker()
        Me.Panel40 = New System.Windows.Forms.Panel()
        Me.Panel39 = New System.Windows.Forms.Panel()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.dg3 = New System.Windows.Forms.DataGridView()
        Me.no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.csid = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.kodeagen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ave = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgWaktu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dt4 = New System.Windows.Forms.DateTimePicker()
        Me.aveall = New System.Windows.Forms.Label()
        Me.cmbcs2 = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.dt3 = New System.Windows.Forms.DateTimePicker()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.cmb_shift1 = New System.Windows.Forms.ComboBox()
        Me.cmb_shift2 = New System.Windows.Forms.ComboBox()
        Me.cmb_shift3 = New System.Windows.Forms.ComboBox()
        Me.cmb_shift4 = New System.Windows.Forms.ComboBox()
        Me.cmb_shift5 = New System.Windows.Forms.ComboBox()
        Me.cmb_shift6 = New System.Windows.Forms.ComboBox()
        Me.strip_istirahat = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DetailIstirahatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dglap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel28.SuspendLayout()
        Me.Panel29.SuspendLayout()
        Me.Panel30.SuspendLayout()
        Me.Panel31.SuspendLayout()
        Me.Panel32.SuspendLayout()
        Me.Panel33.SuspendLayout()
        Me.Panel34.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.Panel20.SuspendLayout()
        CType(Me.dgtab3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        Me.Panel23.SuspendLayout()
        Me.Panel22.SuspendLayout()
        Me.Panel24.SuspendLayout()
        Me.Panel21.SuspendLayout()
        Me.Panel25.SuspendLayout()
        Me.Panel26.SuspendLayout()
        Me.Panel27.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel10.SuspendLayout()
        CType(Me.dg2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.strip_antusias.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel43.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.dgrate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel35.SuspendLayout()
        Me.Panel44.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        CType(Me.dgistirahat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel41.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel15.SuspendLayout()
        CType(Me.dg3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        Me.strip_istirahat.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.AccessibleRole = System.Windows.Forms.AccessibleRole.None
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1291, 432)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel5)
        Me.TabPage1.Controls.Add(Me.Panel4)
        Me.TabPage1.Controls.Add(Me.Panel3)
        Me.TabPage1.Controls.Add(Me.Panel2)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Waktu Salam Awal"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.dglap)
        Me.Panel5.Controls.Add(Me.Button1)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(18, 47)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1247, 337)
        Me.Panel5.TabIndex = 9
        '
        'dglap
        '
        Me.dglap.AllowUserToAddRows = False
        Me.dglap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dglap.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.petugas, Me.setengah, Me.tuju, Me.sepuluh, Me.sebelas, Me.nolsatumenit, Me.noolsatupersen, Me.duabelas, Me.tigabelas, Me.tiga, Me.satulimapersen, Me.tigasatu, Me.delapan, Me.empat, Me.sembilan, Me.enam, Me.dua, Me.satu, Me.lima, Me.rata})
        Me.dglap.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dglap.Location = New System.Drawing.Point(0, 0)
        Me.dglap.Name = "dglap"
        Me.dglap.Size = New System.Drawing.Size(1247, 337)
        Me.dglap.TabIndex = 3
        '
        'petugas
        '
        Me.petugas.HeaderText = "Petugas"
        Me.petugas.Name = "petugas"
        '
        'setengah
        '
        Me.setengah.HeaderText = "0 - 30 detik"
        Me.setengah.Name = "setengah"
        '
        'tuju
        '
        Me.tuju.HeaderText = "%"
        Me.tuju.Name = "tuju"
        '
        'sepuluh
        '
        Me.sepuluh.HeaderText = "0 - 45 detik"
        Me.sepuluh.Name = "sepuluh"
        '
        'sebelas
        '
        Me.sebelas.HeaderText = "%"
        Me.sebelas.Name = "sebelas"
        '
        'nolsatumenit
        '
        Me.nolsatumenit.HeaderText = "0 - 60 Detik"
        Me.nolsatumenit.Name = "nolsatumenit"
        '
        'noolsatupersen
        '
        Me.noolsatupersen.HeaderText = "%"
        Me.noolsatupersen.Name = "noolsatupersen"
        '
        'duabelas
        '
        Me.duabelas.HeaderText = "46 - 60"
        Me.duabelas.Name = "duabelas"
        '
        'tigabelas
        '
        Me.tigabelas.HeaderText = "%"
        Me.tigabelas.Name = "tigabelas"
        '
        'tiga
        '
        Me.tiga.HeaderText = "1 - 5 Menit"
        Me.tiga.Name = "tiga"
        '
        'satulimapersen
        '
        Me.satulimapersen.HeaderText = "%"
        Me.satulimapersen.Name = "satulimapersen"
        '
        'tigasatu
        '
        Me.tigasatu.HeaderText = ">30 Detik & ≤45 Detik"
        Me.tigasatu.Name = "tigasatu"
        Me.tigasatu.Visible = False
        '
        'delapan
        '
        Me.delapan.HeaderText = "%"
        Me.delapan.Name = "delapan"
        Me.delapan.Visible = False
        '
        'empat
        '
        Me.empat.HeaderText = "> 45 Detik & ≤1 Menit"
        Me.empat.Name = "empat"
        Me.empat.Visible = False
        '
        'sembilan
        '
        Me.sembilan.HeaderText = "%"
        Me.sembilan.Name = "sembilan"
        Me.sembilan.Visible = False
        '
        'enam
        '
        Me.enam.HeaderText = ">3 & ≤5 menit"
        Me.enam.Name = "enam"
        Me.enam.Visible = False
        '
        'dua
        '
        Me.dua.HeaderText = ">5 & ≤15 Menit"
        Me.dua.Name = "dua"
        Me.dua.Visible = False
        '
        'satu
        '
        Me.satu.HeaderText = ">15 & ≤30 Menit"
        Me.satu.Name = "satu"
        Me.satu.Visible = False
        '
        'lima
        '
        Me.lima.HeaderText = "> 30 Menit"
        Me.lima.Name = "lima"
        Me.lima.Visible = False
        '
        'rata
        '
        Me.rata.HeaderText = "Jumlah Chat"
        Me.rata.Name = "rata"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(269, 93)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Proses"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.cmb_shift1)
        Me.Panel4.Controls.Add(Me.cmblevel1)
        Me.Panel4.Controls.Add(Me.Panel28)
        Me.Panel4.Controls.Add(Me.Panel29)
        Me.Panel4.Controls.Add(Me.Panel30)
        Me.Panel4.Controls.Add(Me.Panel31)
        Me.Panel4.Controls.Add(Me.Panel32)
        Me.Panel4.Controls.Add(Me.Panel33)
        Me.Panel4.Controls.Add(Me.Panel34)
        Me.Panel4.Controls.Add(Me.Button4)
        Me.Panel4.Controls.Add(Me.Label6)
        Me.Panel4.Controls.Add(Me.dt6)
        Me.Panel4.Controls.Add(Me.Label1)
        Me.Panel4.Controls.Add(Me.dt)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(18, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1247, 44)
        Me.Panel4.TabIndex = 8
        '
        'cmblevel1
        '
        Me.cmblevel1.FormattingEnabled = True
        Me.cmblevel1.Items.AddRange(New Object() {"B", "S"})
        Me.cmblevel1.Location = New System.Drawing.Point(310, 10)
        Me.cmblevel1.Name = "cmblevel1"
        Me.cmblevel1.Size = New System.Drawing.Size(75, 21)
        Me.cmblevel1.TabIndex = 36
        '
        'Panel28
        '
        Me.Panel28.Controls.Add(Me.TextBox1)
        Me.Panel28.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel28.Location = New System.Drawing.Point(687, 0)
        Me.Panel28.Name = "Panel28"
        Me.Panel28.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel28.Size = New System.Drawing.Size(80, 44)
        Me.Panel28.TabIndex = 31
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.Lime
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox1.Enabled = False
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(5, 10)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(65, 20)
        Me.TextBox1.TabIndex = 21
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel29
        '
        Me.Panel29.Controls.Add(Me.TextBox2)
        Me.Panel29.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel29.Location = New System.Drawing.Point(767, 0)
        Me.Panel29.Name = "Panel29"
        Me.Panel29.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel29.Size = New System.Drawing.Size(80, 44)
        Me.Panel29.TabIndex = 30
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(19, Byte), Integer))
        Me.TextBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox2.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(5, 10)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(65, 20)
        Me.TextBox2.TabIndex = 20
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel30
        '
        Me.Panel30.Controls.Add(Me.TextBox3)
        Me.Panel30.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel30.Location = New System.Drawing.Point(847, 0)
        Me.Panel30.Name = "Panel30"
        Me.Panel30.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel30.Size = New System.Drawing.Size(80, 44)
        Me.Panel30.TabIndex = 32
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(133, Byte), Integer))
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.ForeColor = System.Drawing.Color.White
        Me.TextBox3.Location = New System.Drawing.Point(5, 10)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(65, 20)
        Me.TextBox3.TabIndex = 21
        Me.TextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel31
        '
        Me.Panel31.Controls.Add(Me.TextBox4)
        Me.Panel31.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel31.Location = New System.Drawing.Point(927, 0)
        Me.Panel31.Name = "Panel31"
        Me.Panel31.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel31.Size = New System.Drawing.Size(80, 44)
        Me.Panel31.TabIndex = 29
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.TextBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox4.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox4.Enabled = False
        Me.TextBox4.Location = New System.Drawing.Point(5, 10)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(65, 20)
        Me.TextBox4.TabIndex = 19
        Me.TextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel32
        '
        Me.Panel32.Controls.Add(Me.TextBox5)
        Me.Panel32.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel32.Location = New System.Drawing.Point(1007, 0)
        Me.Panel32.Name = "Panel32"
        Me.Panel32.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel32.Size = New System.Drawing.Size(80, 44)
        Me.Panel32.TabIndex = 33
        '
        'TextBox5
        '
        Me.TextBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(26, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TextBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox5.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.ForeColor = System.Drawing.Color.White
        Me.TextBox5.Location = New System.Drawing.Point(5, 10)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(65, 20)
        Me.TextBox5.TabIndex = 21
        Me.TextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel33
        '
        Me.Panel33.Controls.Add(Me.TextBox6)
        Me.Panel33.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel33.Location = New System.Drawing.Point(1087, 0)
        Me.Panel33.Name = "Panel33"
        Me.Panel33.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel33.Size = New System.Drawing.Size(80, 44)
        Me.Panel33.TabIndex = 34
        Me.Panel33.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.TextBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox6.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox6.ForeColor = System.Drawing.Color.White
        Me.TextBox6.Location = New System.Drawing.Point(5, 10)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.ReadOnly = True
        Me.TextBox6.Size = New System.Drawing.Size(65, 20)
        Me.TextBox6.TabIndex = 21
        Me.TextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel34
        '
        Me.Panel34.Controls.Add(Me.TextBox7)
        Me.Panel34.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel34.Location = New System.Drawing.Point(1167, 0)
        Me.Panel34.Name = "Panel34"
        Me.Panel34.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel34.Size = New System.Drawing.Size(80, 44)
        Me.Panel34.TabIndex = 35
        Me.Panel34.Visible = False
        '
        'TextBox7
        '
        Me.TextBox7.BackColor = System.Drawing.Color.FromArgb(CType(CType(77, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TextBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox7.Dock = System.Windows.Forms.DockStyle.Right
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.ForeColor = System.Drawing.Color.White
        Me.TextBox7.Location = New System.Drawing.Point(5, 10)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(65, 20)
        Me.TextBox7.TabIndex = 21
        Me.TextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(512, 9)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 11
        Me.Button4.Text = "Proses"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(172, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(18, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "sd"
        '
        'dt6
        '
        Me.dt6.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt6.Location = New System.Drawing.Point(196, 10)
        Me.dt6.Name = "dt6"
        Me.dt6.Size = New System.Drawing.Size(108, 20)
        Me.dt6.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Tanggal"
        '
        'dt
        '
        Me.dt.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt.Location = New System.Drawing.Point(58, 10)
        Me.dt.Name = "dt"
        Me.dt.Size = New System.Drawing.Size(108, 20)
        Me.dt.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(18, 384)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1247, 19)
        Me.Panel3.TabIndex = 7
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(3, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(15, 400)
        Me.Panel2.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel1.Location = New System.Drawing.Point(1265, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(15, 400)
        Me.Panel1.TabIndex = 5
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.Panel20)
        Me.TabPage4.Controls.Add(Me.Panel16)
        Me.TabPage4.Controls.Add(Me.Panel17)
        Me.TabPage4.Controls.Add(Me.Panel18)
        Me.TabPage4.Controls.Add(Me.Panel19)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Kecepatan Balasan"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'Panel20
        '
        Me.Panel20.Controls.Add(Me.dgtab3)
        Me.Panel20.Controls.Add(Me.Button5)
        Me.Panel20.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel20.Location = New System.Drawing.Point(18, 47)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(1247, 337)
        Me.Panel20.TabIndex = 24
        '
        'dgtab3
        '
        Me.dgtab3.AllowUserToAddRows = False
        Me.dgtab3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgtab3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cst3, Me.kurang30, Me.present3, Me.nolsampe45, Me.persenempat, Me.nolsampesatumenit, Me.nolsatupersen, Me.nolsampesatu, Me.persenlima, Me.limamenit, Me.persent4, Me.limabelas, Me.persent5, Me.tigapuluh, Me.persent6, Me.lebih, Me.persent7, Me.ratat3})
        Me.dgtab3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgtab3.Location = New System.Drawing.Point(0, 0)
        Me.dgtab3.Name = "dgtab3"
        Me.dgtab3.Size = New System.Drawing.Size(1247, 337)
        Me.dgtab3.TabIndex = 4
        '
        'cst3
        '
        Me.cst3.HeaderText = "Petugas"
        Me.cst3.Name = "cst3"
        '
        'kurang30
        '
        Me.kurang30.HeaderText = "0 - 30 detik"
        Me.kurang30.Name = "kurang30"
        '
        'present3
        '
        Me.present3.HeaderText = "%"
        Me.present3.Name = "present3"
        '
        'nolsampe45
        '
        Me.nolsampe45.HeaderText = "0 - 45 detik"
        Me.nolsampe45.Name = "nolsampe45"
        '
        'persenempat
        '
        Me.persenempat.HeaderText = "%"
        Me.persenempat.Name = "persenempat"
        '
        'nolsampesatumenit
        '
        Me.nolsampesatumenit.HeaderText = "0 - 60 Detik"
        Me.nolsampesatumenit.Name = "nolsampesatumenit"
        '
        'nolsatupersen
        '
        Me.nolsatupersen.HeaderText = "%"
        Me.nolsatupersen.Name = "nolsatupersen"
        '
        'nolsampesatu
        '
        Me.nolsampesatu.HeaderText = "46 - 60 Detik"
        Me.nolsampesatu.Name = "nolsampesatu"
        '
        'persenlima
        '
        Me.persenlima.HeaderText = "%"
        Me.persenlima.Name = "persenlima"
        '
        'limamenit
        '
        Me.limamenit.HeaderText = "1 - 5 menit"
        Me.limamenit.Name = "limamenit"
        '
        'persent4
        '
        Me.persent4.HeaderText = "%"
        Me.persent4.Name = "persent4"
        '
        'limabelas
        '
        Me.limabelas.HeaderText = ">5 - 15 Menit"
        Me.limabelas.Name = "limabelas"
        Me.limabelas.Visible = False
        '
        'persent5
        '
        Me.persent5.HeaderText = "%"
        Me.persent5.Name = "persent5"
        Me.persent5.Visible = False
        '
        'tigapuluh
        '
        Me.tigapuluh.HeaderText = ">15 - 30 Menit"
        Me.tigapuluh.Name = "tigapuluh"
        Me.tigapuluh.Visible = False
        '
        'persent6
        '
        Me.persent6.HeaderText = "%"
        Me.persent6.Name = "persent6"
        Me.persent6.Visible = False
        '
        'lebih
        '
        Me.lebih.HeaderText = "> 30 Menit"
        Me.lebih.Name = "lebih"
        Me.lebih.Visible = False
        '
        'persent7
        '
        Me.persent7.HeaderText = "%"
        Me.persent7.Name = "persent7"
        Me.persent7.Visible = False
        '
        'ratat3
        '
        Me.ratat3.HeaderText = "Jumlah Chat"
        Me.ratat3.Name = "ratat3"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(321, 153)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(32, 23)
        Me.Button5.TabIndex = 29
        Me.Button5.Text = "Proses"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Panel16
        '
        Me.Panel16.Controls.Add(Me.cmb_shift2)
        Me.Panel16.Controls.Add(Me.cmblevel2)
        Me.Panel16.Controls.Add(Me.Button7)
        Me.Panel16.Controls.Add(Me.Panel23)
        Me.Panel16.Controls.Add(Me.Panel22)
        Me.Panel16.Controls.Add(Me.Panel24)
        Me.Panel16.Controls.Add(Me.Panel21)
        Me.Panel16.Controls.Add(Me.Panel25)
        Me.Panel16.Controls.Add(Me.Panel26)
        Me.Panel16.Controls.Add(Me.Panel27)
        Me.Panel16.Controls.Add(Me.Label7)
        Me.Panel16.Controls.Add(Me.dtt4)
        Me.Panel16.Controls.Add(Me.Label9)
        Me.Panel16.Controls.Add(Me.dtt3)
        Me.Panel16.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel16.Location = New System.Drawing.Point(18, 3)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(1247, 44)
        Me.Panel16.TabIndex = 23
        '
        'cmblevel2
        '
        Me.cmblevel2.FormattingEnabled = True
        Me.cmblevel2.Items.AddRange(New Object() {"B", "S"})
        Me.cmblevel2.Location = New System.Drawing.Point(310, 10)
        Me.cmblevel2.Name = "cmblevel2"
        Me.cmblevel2.Size = New System.Drawing.Size(60, 21)
        Me.cmblevel2.TabIndex = 37
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(502, 9)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(90, 23)
        Me.Button7.TabIndex = 30
        Me.Button7.Text = "Proses"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Panel23
        '
        Me.Panel23.Controls.Add(Me.warna1)
        Me.Panel23.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel23.Location = New System.Drawing.Point(687, 0)
        Me.Panel23.Name = "Panel23"
        Me.Panel23.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel23.Size = New System.Drawing.Size(80, 44)
        Me.Panel23.TabIndex = 24
        '
        'warna1
        '
        Me.warna1.BackColor = System.Drawing.Color.Lime
        Me.warna1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna1.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna1.Enabled = False
        Me.warna1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna1.Location = New System.Drawing.Point(5, 10)
        Me.warna1.Name = "warna1"
        Me.warna1.ReadOnly = True
        Me.warna1.Size = New System.Drawing.Size(65, 20)
        Me.warna1.TabIndex = 21
        Me.warna1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel22
        '
        Me.Panel22.Controls.Add(Me.warna2)
        Me.Panel22.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel22.Location = New System.Drawing.Point(767, 0)
        Me.Panel22.Name = "Panel22"
        Me.Panel22.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel22.Size = New System.Drawing.Size(80, 44)
        Me.Panel22.TabIndex = 23
        '
        'warna2
        '
        Me.warna2.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(19, Byte), Integer))
        Me.warna2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna2.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna2.Enabled = False
        Me.warna2.Location = New System.Drawing.Point(5, 10)
        Me.warna2.Name = "warna2"
        Me.warna2.ReadOnly = True
        Me.warna2.Size = New System.Drawing.Size(65, 20)
        Me.warna2.TabIndex = 20
        Me.warna2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel24
        '
        Me.Panel24.Controls.Add(Me.warna4)
        Me.Panel24.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel24.Location = New System.Drawing.Point(847, 0)
        Me.Panel24.Name = "Panel24"
        Me.Panel24.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel24.Size = New System.Drawing.Size(80, 44)
        Me.Panel24.TabIndex = 25
        '
        'warna4
        '
        Me.warna4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(133, Byte), Integer))
        Me.warna4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna4.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna4.ForeColor = System.Drawing.Color.White
        Me.warna4.Location = New System.Drawing.Point(5, 10)
        Me.warna4.Name = "warna4"
        Me.warna4.ReadOnly = True
        Me.warna4.Size = New System.Drawing.Size(65, 20)
        Me.warna4.TabIndex = 21
        Me.warna4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel21
        '
        Me.Panel21.Controls.Add(Me.warna3)
        Me.Panel21.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel21.Location = New System.Drawing.Point(927, 0)
        Me.Panel21.Name = "Panel21"
        Me.Panel21.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel21.Size = New System.Drawing.Size(80, 44)
        Me.Panel21.TabIndex = 22
        '
        'warna3
        '
        Me.warna3.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.warna3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna3.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna3.Enabled = False
        Me.warna3.Location = New System.Drawing.Point(5, 10)
        Me.warna3.Name = "warna3"
        Me.warna3.ReadOnly = True
        Me.warna3.Size = New System.Drawing.Size(65, 20)
        Me.warna3.TabIndex = 19
        Me.warna3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel25
        '
        Me.Panel25.Controls.Add(Me.warna5)
        Me.Panel25.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel25.Location = New System.Drawing.Point(1007, 0)
        Me.Panel25.Name = "Panel25"
        Me.Panel25.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel25.Size = New System.Drawing.Size(80, 44)
        Me.Panel25.TabIndex = 26
        '
        'warna5
        '
        Me.warna5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(26, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.warna5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna5.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna5.ForeColor = System.Drawing.Color.White
        Me.warna5.Location = New System.Drawing.Point(5, 10)
        Me.warna5.Name = "warna5"
        Me.warna5.ReadOnly = True
        Me.warna5.Size = New System.Drawing.Size(65, 20)
        Me.warna5.TabIndex = 21
        Me.warna5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel26
        '
        Me.Panel26.Controls.Add(Me.warna6)
        Me.Panel26.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel26.Location = New System.Drawing.Point(1087, 0)
        Me.Panel26.Name = "Panel26"
        Me.Panel26.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel26.Size = New System.Drawing.Size(80, 44)
        Me.Panel26.TabIndex = 27
        Me.Panel26.Visible = False
        '
        'warna6
        '
        Me.warna6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(92, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.warna6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna6.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna6.ForeColor = System.Drawing.Color.White
        Me.warna6.Location = New System.Drawing.Point(5, 10)
        Me.warna6.Name = "warna6"
        Me.warna6.ReadOnly = True
        Me.warna6.Size = New System.Drawing.Size(65, 20)
        Me.warna6.TabIndex = 21
        Me.warna6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Panel27
        '
        Me.Panel27.Controls.Add(Me.warna7)
        Me.Panel27.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel27.Location = New System.Drawing.Point(1167, 0)
        Me.Panel27.Name = "Panel27"
        Me.Panel27.Padding = New System.Windows.Forms.Padding(0, 10, 10, 0)
        Me.Panel27.Size = New System.Drawing.Size(80, 44)
        Me.Panel27.TabIndex = 28
        Me.Panel27.Visible = False
        '
        'warna7
        '
        Me.warna7.BackColor = System.Drawing.Color.FromArgb(CType(CType(77, Byte), Integer), CType(CType(77, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.warna7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.warna7.Dock = System.Windows.Forms.DockStyle.Right
        Me.warna7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.warna7.ForeColor = System.Drawing.Color.White
        Me.warna7.Location = New System.Drawing.Point(5, 10)
        Me.warna7.Name = "warna7"
        Me.warna7.ReadOnly = True
        Me.warna7.Size = New System.Drawing.Size(65, 20)
        Me.warna7.TabIndex = 21
        Me.warna7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(172, 12)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(18, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "sd"
        '
        'dtt4
        '
        Me.dtt4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtt4.Location = New System.Drawing.Point(196, 10)
        Me.dtt4.Name = "dtt4"
        Me.dtt4.Size = New System.Drawing.Size(108, 20)
        Me.dtt4.TabIndex = 7
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 14)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(46, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Tanggal"
        '
        'dtt3
        '
        Me.dtt3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtt3.Location = New System.Drawing.Point(58, 10)
        Me.dtt3.Name = "dtt3"
        Me.dtt3.Size = New System.Drawing.Size(108, 20)
        Me.dtt3.TabIndex = 2
        '
        'Panel17
        '
        Me.Panel17.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel17.Location = New System.Drawing.Point(1265, 3)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(15, 381)
        Me.Panel17.TabIndex = 20
        '
        'Panel18
        '
        Me.Panel18.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel18.Location = New System.Drawing.Point(18, 384)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(1262, 19)
        Me.Panel18.TabIndex = 22
        '
        'Panel19
        '
        Me.Panel19.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel19.Location = New System.Drawing.Point(3, 3)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(15, 400)
        Me.Panel19.TabIndex = 21
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel10)
        Me.TabPage2.Controls.Add(Me.Panel6)
        Me.TabPage2.Controls.Add(Me.Panel7)
        Me.TabPage2.Controls.Add(Me.Panel8)
        Me.TabPage2.Controls.Add(Me.Panel9)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Antusias Balasan"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.dg2)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel10.Location = New System.Drawing.Point(18, 47)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(1247, 337)
        Me.Panel10.TabIndex = 14
        '
        'dg2
        '
        Me.dg2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cs, Me.agen, Me.pesan, Me.tanggal, Me.antusias, Me.cuek, Me.persen})
        Me.dg2.ContextMenuStrip = Me.strip_antusias
        Me.dg2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg2.Location = New System.Drawing.Point(0, 0)
        Me.dg2.Name = "dg2"
        Me.dg2.Size = New System.Drawing.Size(1247, 337)
        Me.dg2.TabIndex = 10
        '
        'cs
        '
        Me.cs.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cs.HeaderText = "Petugas"
        Me.cs.Name = "cs"
        '
        'agen
        '
        Me.agen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.agen.HeaderText = "Kode Agen"
        Me.agen.Name = "agen"
        Me.agen.Visible = False
        '
        'pesan
        '
        Me.pesan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.pesan.HeaderText = "Pesan"
        Me.pesan.Name = "pesan"
        Me.pesan.Visible = False
        '
        'tanggal
        '
        Me.tanggal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.tanggal.HeaderText = "Tanggal"
        Me.tanggal.Name = "tanggal"
        Me.tanggal.Visible = False
        '
        'antusias
        '
        Me.antusias.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.antusias.HeaderText = "Antusias"
        Me.antusias.Name = "antusias"
        '
        'cuek
        '
        Me.cuek.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.cuek.HeaderText = "Cuek"
        Me.cuek.Name = "cuek"
        '
        'persen
        '
        Me.persen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.persen.HeaderText = "Persentase Cuek"
        Me.persen.Name = "persen"
        '
        'strip_antusias
        '
        Me.strip_antusias.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetailToolStripMenuItem, Me.DetailCuekToolStripMenuItem})
        Me.strip_antusias.Name = "ContextMenuStrip1"
        Me.strip_antusias.Size = New System.Drawing.Size(153, 48)
        '
        'DetailToolStripMenuItem
        '
        Me.DetailToolStripMenuItem.Name = "DetailToolStripMenuItem"
        Me.DetailToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DetailToolStripMenuItem.Text = "Detail Antusias"
        '
        'DetailCuekToolStripMenuItem
        '
        Me.DetailCuekToolStripMenuItem.Name = "DetailCuekToolStripMenuItem"
        Me.DetailCuekToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DetailCuekToolStripMenuItem.Text = "Detail Cuek"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.cmb_shift3)
        Me.Panel6.Controls.Add(Me.cmblevel3)
        Me.Panel6.Controls.Add(Me.Panel43)
        Me.Panel6.Controls.Add(Me.Label5)
        Me.Panel6.Controls.Add(Me.dt5)
        Me.Panel6.Controls.Add(Me.Label2)
        Me.Panel6.Controls.Add(Me.Button2)
        Me.Panel6.Controls.Add(Me.dt2)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel6.Location = New System.Drawing.Point(18, 3)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(1247, 44)
        Me.Panel6.TabIndex = 13
        '
        'cmblevel3
        '
        Me.cmblevel3.FormattingEnabled = True
        Me.cmblevel3.Items.AddRange(New Object() {"B", "S"})
        Me.cmblevel3.Location = New System.Drawing.Point(310, 10)
        Me.cmblevel3.Name = "cmblevel3"
        Me.cmblevel3.Size = New System.Drawing.Size(66, 21)
        Me.cmblevel3.TabIndex = 39
        '
        'Panel43
        '
        Me.Panel43.Controls.Add(Me.TextBox9)
        Me.Panel43.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel43.Location = New System.Drawing.Point(680, 0)
        Me.Panel43.Name = "Panel43"
        Me.Panel43.Padding = New System.Windows.Forms.Padding(10, 10, 10, 0)
        Me.Panel43.Size = New System.Drawing.Size(567, 44)
        Me.Panel43.TabIndex = 38
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(19, Byte), Integer))
        Me.TextBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox9.Dock = System.Windows.Forms.DockStyle.Left
        Me.TextBox9.Enabled = False
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(10, 10)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.ReadOnly = True
        Me.TextBox9.Size = New System.Drawing.Size(65, 23)
        Me.TextBox9.TabIndex = 21
        Me.TextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(172, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(18, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "sd"
        '
        'dt5
        '
        Me.dt5.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt5.Location = New System.Drawing.Point(196, 10)
        Me.dt5.Name = "dt5"
        Me.dt5.Size = New System.Drawing.Size(108, 20)
        Me.dt5.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Tanggal"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(503, 9)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Proses"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'dt2
        '
        Me.dt2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt2.Location = New System.Drawing.Point(58, 10)
        Me.dt2.Name = "dt2"
        Me.dt2.Size = New System.Drawing.Size(108, 20)
        Me.dt2.TabIndex = 2
        '
        'Panel7
        '
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel7.Location = New System.Drawing.Point(1265, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(15, 381)
        Me.Panel7.TabIndex = 10
        '
        'Panel8
        '
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel8.Location = New System.Drawing.Point(18, 384)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(1262, 19)
        Me.Panel8.TabIndex = 12
        '
        'Panel9
        '
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel9.Location = New System.Drawing.Point(3, 3)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(15, 400)
        Me.Panel9.TabIndex = 11
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.dgrate)
        Me.TabPage6.Controls.Add(Me.Panel35)
        Me.TabPage6.Controls.Add(Me.Panel38)
        Me.TabPage6.Controls.Add(Me.Panel37)
        Me.TabPage6.Controls.Add(Me.Panel36)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Rating CS"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'dgrate
        '
        Me.dgrate.AllowUserToAddRows = False
        Me.dgrate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgrate.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ratecs, Me.rate, Me.rate1, Me.rate2, Me.rate3, Me.rate4, Me.rate5, Me.jmlrate})
        Me.dgrate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgrate.Location = New System.Drawing.Point(11, 50)
        Me.dgrate.Name = "dgrate"
        Me.dgrate.Size = New System.Drawing.Size(1261, 346)
        Me.dgrate.TabIndex = 4
        '
        'ratecs
        '
        Me.ratecs.HeaderText = "Petugas"
        Me.ratecs.Name = "ratecs"
        '
        'rate
        '
        Me.rate.HeaderText = "Rate Cs"
        Me.rate.Name = "rate"
        '
        'rate1
        '
        DataGridViewCellStyle11.NullValue = "0"
        Me.rate1.DefaultCellStyle = DataGridViewCellStyle11
        Me.rate1.HeaderText = "Rate 1"
        Me.rate1.Name = "rate1"
        '
        'rate2
        '
        DataGridViewCellStyle12.NullValue = "0"
        Me.rate2.DefaultCellStyle = DataGridViewCellStyle12
        Me.rate2.HeaderText = "Rate 2"
        Me.rate2.Name = "rate2"
        '
        'rate3
        '
        DataGridViewCellStyle13.NullValue = "0"
        Me.rate3.DefaultCellStyle = DataGridViewCellStyle13
        Me.rate3.HeaderText = "Rate 3"
        Me.rate3.Name = "rate3"
        '
        'rate4
        '
        DataGridViewCellStyle14.NullValue = "0"
        Me.rate4.DefaultCellStyle = DataGridViewCellStyle14
        Me.rate4.HeaderText = "Rate 4"
        Me.rate4.Name = "rate4"
        '
        'rate5
        '
        DataGridViewCellStyle15.NullValue = "0"
        Me.rate5.DefaultCellStyle = DataGridViewCellStyle15
        Me.rate5.HeaderText = "Rate 5"
        Me.rate5.Name = "rate5"
        '
        'jmlrate
        '
        Me.jmlrate.HeaderText = "Total Rate Chat"
        Me.jmlrate.Name = "jmlrate"
        '
        'Panel35
        '
        Me.Panel35.Controls.Add(Me.cmb_shift4)
        Me.Panel35.Controls.Add(Me.cmblevel4)
        Me.Panel35.Controls.Add(Me.Panel44)
        Me.Panel35.Controls.Add(Me.Button8)
        Me.Panel35.Controls.Add(Me.Label8)
        Me.Panel35.Controls.Add(Me.dtrate2)
        Me.Panel35.Controls.Add(Me.Label10)
        Me.Panel35.Controls.Add(Me.dtrate1)
        Me.Panel35.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel35.Location = New System.Drawing.Point(11, 0)
        Me.Panel35.Name = "Panel35"
        Me.Panel35.Size = New System.Drawing.Size(1261, 50)
        Me.Panel35.TabIndex = 0
        '
        'cmblevel4
        '
        Me.cmblevel4.FormattingEnabled = True
        Me.cmblevel4.Items.AddRange(New Object() {"B", "S"})
        Me.cmblevel4.Location = New System.Drawing.Point(320, 13)
        Me.cmblevel4.Name = "cmblevel4"
        Me.cmblevel4.Size = New System.Drawing.Size(66, 21)
        Me.cmblevel4.TabIndex = 40
        '
        'Panel44
        '
        Me.Panel44.Controls.Add(Me.TextBox8)
        Me.Panel44.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel44.Location = New System.Drawing.Point(694, 0)
        Me.Panel44.Name = "Panel44"
        Me.Panel44.Padding = New System.Windows.Forms.Padding(10, 10, 10, 0)
        Me.Panel44.Size = New System.Drawing.Size(567, 50)
        Me.Panel44.TabIndex = 37
        '
        'TextBox8
        '
        Me.TextBox8.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(236, Byte), Integer), CType(CType(19, Byte), Integer))
        Me.TextBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox8.Dock = System.Windows.Forms.DockStyle.Left
        Me.TextBox8.Enabled = False
        Me.TextBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox8.Location = New System.Drawing.Point(10, 10)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.ReadOnly = True
        Me.TextBox8.Size = New System.Drawing.Size(65, 23)
        Me.TextBox8.TabIndex = 21
        Me.TextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(515, 12)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(72, 23)
        Me.Button8.TabIndex = 35
        Me.Button8.Text = "Proses"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(179, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(18, 13)
        Me.Label8.TabIndex = 34
        Me.Label8.Text = "sd"
        '
        'dtrate2
        '
        Me.dtrate2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtrate2.Location = New System.Drawing.Point(203, 13)
        Me.dtrate2.Name = "dtrate2"
        Me.dtrate2.Size = New System.Drawing.Size(108, 20)
        Me.dtrate2.TabIndex = 33
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 17)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(46, 13)
        Me.Label10.TabIndex = 31
        Me.Label10.Text = "Tanggal"
        '
        'dtrate1
        '
        Me.dtrate1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtrate1.Location = New System.Drawing.Point(65, 13)
        Me.dtrate1.Name = "dtrate1"
        Me.dtrate1.Size = New System.Drawing.Size(108, 20)
        Me.dtrate1.TabIndex = 32
        '
        'Panel38
        '
        Me.Panel38.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel38.Location = New System.Drawing.Point(1272, 0)
        Me.Panel38.Name = "Panel38"
        Me.Panel38.Size = New System.Drawing.Size(11, 396)
        Me.Panel38.TabIndex = 3
        '
        'Panel37
        '
        Me.Panel37.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel37.Location = New System.Drawing.Point(0, 0)
        Me.Panel37.Name = "Panel37"
        Me.Panel37.Size = New System.Drawing.Size(11, 396)
        Me.Panel37.TabIndex = 2
        '
        'Panel36
        '
        Me.Panel36.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel36.Location = New System.Drawing.Point(0, 396)
        Me.Panel36.Name = "Panel36"
        Me.Panel36.Size = New System.Drawing.Size(1283, 10)
        Me.Panel36.TabIndex = 1
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.dgistirahat)
        Me.TabPage7.Controls.Add(Me.Panel42)
        Me.TabPage7.Controls.Add(Me.Panel41)
        Me.TabPage7.Controls.Add(Me.Panel40)
        Me.TabPage7.Controls.Add(Me.Panel39)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage7.TabIndex = 6
        Me.TabPage7.Text = "Istirahat"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'dgistirahat
        '
        Me.dgistirahat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgistirahat.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cspetugas, Me.offistirahat, Me.jml1, Me.waktuistirahat, Me.jml2, Me.break, Me.jml3, Me.ibadah, Me.jml4})
        Me.dgistirahat.ContextMenuStrip = Me.strip_istirahat
        Me.dgistirahat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgistirahat.Location = New System.Drawing.Point(11, 50)
        Me.dgistirahat.Name = "dgistirahat"
        Me.dgistirahat.Size = New System.Drawing.Size(1261, 346)
        Me.dgistirahat.TabIndex = 7
        '
        'cspetugas
        '
        Me.cspetugas.HeaderText = "Petugas"
        Me.cspetugas.Name = "cspetugas"
        '
        'offistirahat
        '
        Me.offistirahat.HeaderText = "Off Antrian Istirahat"
        Me.offistirahat.Name = "offistirahat"
        '
        'jml1
        '
        Me.jml1.HeaderText = "Jumlah OFF Istirahat"
        Me.jml1.Name = "jml1"
        '
        'waktuistirahat
        '
        Me.waktuistirahat.HeaderText = "Istirahat"
        Me.waktuistirahat.Name = "waktuistirahat"
        '
        'jml2
        '
        Me.jml2.HeaderText = "Jumlah Istirahat"
        Me.jml2.Name = "jml2"
        '
        'break
        '
        Me.break.HeaderText = "Break"
        Me.break.Name = "break"
        '
        'jml3
        '
        Me.jml3.HeaderText = "Jumlah Break"
        Me.jml3.Name = "jml3"
        '
        'ibadah
        '
        Me.ibadah.HeaderText = "Ibadah"
        Me.ibadah.Name = "ibadah"
        '
        'jml4
        '
        Me.jml4.HeaderText = "Jumlah Ibadah"
        Me.jml4.Name = "jml4"
        '
        'Panel42
        '
        Me.Panel42.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel42.Location = New System.Drawing.Point(11, 396)
        Me.Panel42.Name = "Panel42"
        Me.Panel42.Size = New System.Drawing.Size(1261, 10)
        Me.Panel42.TabIndex = 6
        '
        'Panel41
        '
        Me.Panel41.Controls.Add(Me.cmb_shift5)
        Me.Panel41.Controls.Add(Me.cmblevel5)
        Me.Panel41.Controls.Add(Me.Button9)
        Me.Panel41.Controls.Add(Me.Label11)
        Me.Panel41.Controls.Add(Me.dtisti2)
        Me.Panel41.Controls.Add(Me.Label12)
        Me.Panel41.Controls.Add(Me.dtisti1)
        Me.Panel41.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel41.Location = New System.Drawing.Point(11, 0)
        Me.Panel41.Name = "Panel41"
        Me.Panel41.Size = New System.Drawing.Size(1261, 50)
        Me.Panel41.TabIndex = 5
        '
        'cmblevel5
        '
        Me.cmblevel5.FormattingEnabled = True
        Me.cmblevel5.Items.AddRange(New Object() {"B", "S"})
        Me.cmblevel5.Location = New System.Drawing.Point(317, 13)
        Me.cmblevel5.Name = "cmblevel5"
        Me.cmblevel5.Size = New System.Drawing.Size(66, 21)
        Me.cmblevel5.TabIndex = 41
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(519, 12)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(90, 23)
        Me.Button9.TabIndex = 35
        Me.Button9.Text = "Proses"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(179, 15)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(18, 13)
        Me.Label11.TabIndex = 34
        Me.Label11.Text = "sd"
        '
        'dtisti2
        '
        Me.dtisti2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtisti2.Location = New System.Drawing.Point(203, 13)
        Me.dtisti2.Name = "dtisti2"
        Me.dtisti2.Size = New System.Drawing.Size(108, 20)
        Me.dtisti2.TabIndex = 33
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(13, 17)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(46, 13)
        Me.Label12.TabIndex = 31
        Me.Label12.Text = "Tanggal"
        '
        'dtisti1
        '
        Me.dtisti1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtisti1.Location = New System.Drawing.Point(65, 13)
        Me.dtisti1.Name = "dtisti1"
        Me.dtisti1.Size = New System.Drawing.Size(108, 20)
        Me.dtisti1.TabIndex = 32
        '
        'Panel40
        '
        Me.Panel40.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel40.Location = New System.Drawing.Point(1272, 0)
        Me.Panel40.Name = "Panel40"
        Me.Panel40.Size = New System.Drawing.Size(11, 406)
        Me.Panel40.TabIndex = 4
        '
        'Panel39
        '
        Me.Panel39.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel39.Location = New System.Drawing.Point(0, 0)
        Me.Panel39.Name = "Panel39"
        Me.Panel39.Size = New System.Drawing.Size(11, 406)
        Me.Panel39.TabIndex = 3
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Panel15)
        Me.TabPage3.Controls.Add(Me.Panel11)
        Me.TabPage3.Controls.Add(Me.Panel12)
        Me.TabPage3.Controls.Add(Me.Panel13)
        Me.TabPage3.Controls.Add(Me.Panel14)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1283, 406)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Rata Rata Waktu Balas"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Panel15
        '
        Me.Panel15.Controls.Add(Me.dg3)
        Me.Panel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel15.Location = New System.Drawing.Point(18, 47)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(1247, 337)
        Me.Panel15.TabIndex = 19
        '
        'dg3
        '
        Me.dg3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dg3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.no, Me.csid, Me.kodeagen, Me.ave, Me.dgWaktu})
        Me.dg3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dg3.Location = New System.Drawing.Point(0, 0)
        Me.dg3.Name = "dg3"
        Me.dg3.Size = New System.Drawing.Size(1247, 337)
        Me.dg3.TabIndex = 11
        '
        'no
        '
        Me.no.HeaderText = "No"
        Me.no.Name = "no"
        '
        'csid
        '
        Me.csid.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.csid.HeaderText = "Petugas"
        Me.csid.Name = "csid"
        '
        'kodeagen
        '
        Me.kodeagen.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.kodeagen.HeaderText = "Kode Agen"
        Me.kodeagen.Name = "kodeagen"
        '
        'ave
        '
        Me.ave.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ave.HeaderText = "Rata Rata"
        Me.ave.Name = "ave"
        '
        'dgWaktu
        '
        Me.dgWaktu.HeaderText = "Column1"
        Me.dgWaktu.Name = "dgWaktu"
        Me.dgWaktu.Visible = False
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.cmb_shift6)
        Me.Panel11.Controls.Add(Me.Label4)
        Me.Panel11.Controls.Add(Me.dt4)
        Me.Panel11.Controls.Add(Me.aveall)
        Me.Panel11.Controls.Add(Me.cmbcs2)
        Me.Panel11.Controls.Add(Me.Label3)
        Me.Panel11.Controls.Add(Me.Button3)
        Me.Panel11.Controls.Add(Me.dt3)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel11.Location = New System.Drawing.Point(18, 3)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(1247, 44)
        Me.Panel11.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(172, 12)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(18, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "sd"
        '
        'dt4
        '
        Me.dt4.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt4.Location = New System.Drawing.Point(196, 10)
        Me.dt4.Name = "dt4"
        Me.dt4.Size = New System.Drawing.Size(108, 20)
        Me.dt4.TabIndex = 7
        '
        'aveall
        '
        Me.aveall.AutoSize = True
        Me.aveall.Dock = System.Windows.Forms.DockStyle.Right
        Me.aveall.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.aveall.Location = New System.Drawing.Point(1176, 0)
        Me.aveall.Name = "aveall"
        Me.aveall.Padding = New System.Windows.Forms.Padding(0, 10, 10, 10)
        Me.aveall.Size = New System.Drawing.Size(71, 40)
        Me.aveall.TabIndex = 6
        Me.aveall.Text = "0 Menit"
        '
        'cmbcs2
        '
        Me.cmbcs2.FormattingEnabled = True
        Me.cmbcs2.Items.AddRange(New Object() {"all"})
        Me.cmbcs2.Location = New System.Drawing.Point(315, 9)
        Me.cmbcs2.Name = "cmbcs2"
        Me.cmbcs2.Size = New System.Drawing.Size(121, 21)
        Me.cmbcs2.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 14)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Tanggal"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(568, 8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 4
        Me.Button3.Text = "Proses"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'dt3
        '
        Me.dt3.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dt3.Location = New System.Drawing.Point(58, 10)
        Me.dt3.Name = "dt3"
        Me.dt3.Size = New System.Drawing.Size(108, 20)
        Me.dt3.TabIndex = 2
        '
        'Panel12
        '
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel12.Location = New System.Drawing.Point(1265, 3)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(15, 381)
        Me.Panel12.TabIndex = 14
        '
        'Panel13
        '
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel13.Location = New System.Drawing.Point(18, 384)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(1262, 19)
        Me.Panel13.TabIndex = 17
        '
        'Panel14
        '
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel14.Location = New System.Drawing.Point(3, 3)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(15, 400)
        Me.Panel14.TabIndex = 16
        '
        'cmb_shift1
        '
        Me.cmb_shift1.FormattingEnabled = True
        Me.cmb_shift1.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift1.Location = New System.Drawing.Point(390, 10)
        Me.cmb_shift1.Name = "cmb_shift1"
        Me.cmb_shift1.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift1.TabIndex = 37
        '
        'cmb_shift2
        '
        Me.cmb_shift2.FormattingEnabled = True
        Me.cmb_shift2.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift2.Location = New System.Drawing.Point(375, 10)
        Me.cmb_shift2.Name = "cmb_shift2"
        Me.cmb_shift2.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift2.TabIndex = 38
        '
        'cmb_shift3
        '
        Me.cmb_shift3.FormattingEnabled = True
        Me.cmb_shift3.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift3.Location = New System.Drawing.Point(380, 10)
        Me.cmb_shift3.Name = "cmb_shift3"
        Me.cmb_shift3.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift3.TabIndex = 40
        '
        'cmb_shift4
        '
        Me.cmb_shift4.FormattingEnabled = True
        Me.cmb_shift4.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift4.Location = New System.Drawing.Point(392, 13)
        Me.cmb_shift4.Name = "cmb_shift4"
        Me.cmb_shift4.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift4.TabIndex = 41
        '
        'cmb_shift5
        '
        Me.cmb_shift5.FormattingEnabled = True
        Me.cmb_shift5.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift5.Location = New System.Drawing.Point(393, 13)
        Me.cmb_shift5.Name = "cmb_shift5"
        Me.cmb_shift5.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift5.TabIndex = 42
        '
        'cmb_shift6
        '
        Me.cmb_shift6.FormattingEnabled = True
        Me.cmb_shift6.Items.AddRange(New Object() {"- all -"})
        Me.cmb_shift6.Location = New System.Drawing.Point(442, 9)
        Me.cmb_shift6.Name = "cmb_shift6"
        Me.cmb_shift6.Size = New System.Drawing.Size(118, 21)
        Me.cmb_shift6.TabIndex = 43
        '
        'strip_istirahat
        '
        Me.strip_istirahat.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DetailIstirahatToolStripMenuItem})
        Me.strip_istirahat.Name = "strip_istirahat"
        Me.strip_istirahat.Size = New System.Drawing.Size(150, 26)
        '
        'DetailIstirahatToolStripMenuItem
        '
        Me.DetailIstirahatToolStripMenuItem.Name = "DetailIstirahatToolStripMenuItem"
        Me.DetailIstirahatToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DetailIstirahatToolStripMenuItem.Text = "Detail Istirahat"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1291, 432)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form1"
        Me.Text = "Performa Cs"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.dglap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel28.ResumeLayout(False)
        Me.Panel28.PerformLayout()
        Me.Panel29.ResumeLayout(False)
        Me.Panel29.PerformLayout()
        Me.Panel30.ResumeLayout(False)
        Me.Panel30.PerformLayout()
        Me.Panel31.ResumeLayout(False)
        Me.Panel31.PerformLayout()
        Me.Panel32.ResumeLayout(False)
        Me.Panel32.PerformLayout()
        Me.Panel33.ResumeLayout(False)
        Me.Panel33.PerformLayout()
        Me.Panel34.ResumeLayout(False)
        Me.Panel34.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.Panel20.ResumeLayout(False)
        CType(Me.dgtab3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        Me.Panel23.ResumeLayout(False)
        Me.Panel23.PerformLayout()
        Me.Panel22.ResumeLayout(False)
        Me.Panel22.PerformLayout()
        Me.Panel24.ResumeLayout(False)
        Me.Panel24.PerformLayout()
        Me.Panel21.ResumeLayout(False)
        Me.Panel21.PerformLayout()
        Me.Panel25.ResumeLayout(False)
        Me.Panel25.PerformLayout()
        Me.Panel26.ResumeLayout(False)
        Me.Panel26.PerformLayout()
        Me.Panel27.ResumeLayout(False)
        Me.Panel27.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        CType(Me.dg2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.strip_antusias.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel43.ResumeLayout(False)
        Me.Panel43.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        CType(Me.dgrate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel35.ResumeLayout(False)
        Me.Panel35.PerformLayout()
        Me.Panel44.ResumeLayout(False)
        Me.Panel44.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        CType(Me.dgistirahat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel41.ResumeLayout(False)
        Me.Panel41.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel15.ResumeLayout(False)
        CType(Me.dg3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        Me.strip_istirahat.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Panel5 As Panel
    Friend WithEvents dglap As DataGridView
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Label1 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents dt As DateTimePicker
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents dt2 As DateTimePicker
    Friend WithEvents Panel7 As Panel
    Friend WithEvents Panel8 As Panel
    Friend WithEvents Panel9 As Panel
    Friend WithEvents Panel10 As Panel
    Friend WithEvents dg2 As DataGridView
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents Panel15 As Panel
    Friend WithEvents Panel11 As Panel
    Friend WithEvents cmbcs2 As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents dt3 As DateTimePicker
    Friend WithEvents Panel12 As Panel
    Friend WithEvents Panel13 As Panel
    Friend WithEvents Panel14 As Panel
    Friend WithEvents dg3 As DataGridView
    Friend WithEvents aveall As Label
    Friend WithEvents strip_antusias As ContextMenuStrip
    Friend WithEvents DetailToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DetailCuekToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents cs As DataGridViewTextBoxColumn
    Friend WithEvents agen As DataGridViewTextBoxColumn
    Friend WithEvents pesan As DataGridViewTextBoxColumn
    Friend WithEvents tanggal As DataGridViewTextBoxColumn
    Friend WithEvents antusias As DataGridViewTextBoxColumn
    Friend WithEvents cuek As DataGridViewTextBoxColumn
    Friend WithEvents persen As DataGridViewTextBoxColumn
    Friend WithEvents Label4 As Label
    Friend WithEvents dt4 As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents dt5 As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents dt6 As DateTimePicker
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents Panel20 As Panel
    Friend WithEvents Panel16 As Panel
    Friend WithEvents Label7 As Label
    Friend WithEvents dtt4 As DateTimePicker
    Friend WithEvents Label9 As Label
    Friend WithEvents dtt3 As DateTimePicker
    Friend WithEvents Panel17 As Panel
    Friend WithEvents Panel18 As Panel
    Friend WithEvents Panel19 As Panel
    Friend WithEvents dgtab3 As DataGridView
    Friend WithEvents warna1 As TextBox
    Friend WithEvents warna2 As TextBox
    Friend WithEvents warna3 As TextBox
    Friend WithEvents Panel23 As Panel
    Friend WithEvents Panel22 As Panel
    Friend WithEvents Panel21 As Panel
    Friend WithEvents Panel24 As Panel
    Friend WithEvents warna4 As TextBox
    Friend WithEvents Panel25 As Panel
    Friend WithEvents warna5 As TextBox
    Friend WithEvents Panel26 As Panel
    Friend WithEvents warna6 As TextBox
    Friend WithEvents Panel27 As Panel
    Friend WithEvents warna7 As TextBox
    Friend WithEvents Button5 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents cst3 As DataGridViewTextBoxColumn
    Friend WithEvents kurang30 As DataGridViewTextBoxColumn
    Friend WithEvents present3 As DataGridViewTextBoxColumn
    Friend WithEvents nolsampe45 As DataGridViewTextBoxColumn
    Friend WithEvents persenempat As DataGridViewTextBoxColumn
    Friend WithEvents nolsampesatumenit As DataGridViewTextBoxColumn
    Friend WithEvents nolsatupersen As DataGridViewTextBoxColumn
    Friend WithEvents nolsampesatu As DataGridViewTextBoxColumn
    Friend WithEvents persenlima As DataGridViewTextBoxColumn
    Friend WithEvents limamenit As DataGridViewTextBoxColumn
    Friend WithEvents persent4 As DataGridViewTextBoxColumn
    Friend WithEvents limabelas As DataGridViewTextBoxColumn
    Friend WithEvents persent5 As DataGridViewTextBoxColumn
    Friend WithEvents tigapuluh As DataGridViewTextBoxColumn
    Friend WithEvents persent6 As DataGridViewTextBoxColumn
    Friend WithEvents lebih As DataGridViewTextBoxColumn
    Friend WithEvents persent7 As DataGridViewTextBoxColumn
    Friend WithEvents ratat3 As DataGridViewTextBoxColumn
    Friend WithEvents Panel28 As Panel
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Panel29 As Panel
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Panel30 As Panel
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Panel31 As Panel
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Panel32 As Panel
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Panel33 As Panel
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents Panel34 As Panel
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents Panel38 As Panel
    Friend WithEvents Panel37 As Panel
    Friend WithEvents Panel36 As Panel
    Friend WithEvents Panel35 As Panel
    Friend WithEvents Button8 As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents dtrate2 As DateTimePicker
    Friend WithEvents Label10 As Label
    Friend WithEvents dtrate1 As DateTimePicker
    Friend WithEvents dgrate As DataGridView
    Friend WithEvents TabPage7 As TabPage
    Friend WithEvents dgistirahat As DataGridView
    Friend WithEvents Panel42 As Panel
    Friend WithEvents Panel41 As Panel
    Friend WithEvents Button9 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents dtisti2 As DateTimePicker
    Friend WithEvents Label12 As Label
    Friend WithEvents dtisti1 As DateTimePicker
    Friend WithEvents Panel40 As Panel
    Friend WithEvents Panel39 As Panel
    Friend WithEvents petugas As DataGridViewTextBoxColumn
    Friend WithEvents setengah As DataGridViewTextBoxColumn
    Friend WithEvents tuju As DataGridViewTextBoxColumn
    Friend WithEvents sepuluh As DataGridViewTextBoxColumn
    Friend WithEvents sebelas As DataGridViewTextBoxColumn
    Friend WithEvents nolsatumenit As DataGridViewTextBoxColumn
    Friend WithEvents noolsatupersen As DataGridViewTextBoxColumn
    Friend WithEvents duabelas As DataGridViewTextBoxColumn
    Friend WithEvents tigabelas As DataGridViewTextBoxColumn
    Friend WithEvents tiga As DataGridViewTextBoxColumn
    Friend WithEvents satulimapersen As DataGridViewTextBoxColumn
    Friend WithEvents tigasatu As DataGridViewTextBoxColumn
    Friend WithEvents delapan As DataGridViewTextBoxColumn
    Friend WithEvents empat As DataGridViewTextBoxColumn
    Friend WithEvents sembilan As DataGridViewTextBoxColumn
    Friend WithEvents enam As DataGridViewTextBoxColumn
    Friend WithEvents dua As DataGridViewTextBoxColumn
    Friend WithEvents satu As DataGridViewTextBoxColumn
    Friend WithEvents lima As DataGridViewTextBoxColumn
    Friend WithEvents rata As DataGridViewTextBoxColumn
    Friend WithEvents Panel44 As Panel
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Panel43 As Panel
    Friend WithEvents TextBox9 As TextBox
    Friend WithEvents no As DataGridViewTextBoxColumn
    Friend WithEvents csid As DataGridViewTextBoxColumn
    Friend WithEvents kodeagen As DataGridViewTextBoxColumn
    Friend WithEvents ave As DataGridViewTextBoxColumn
    Friend WithEvents dgWaktu As DataGridViewTextBoxColumn
    Friend WithEvents ratecs As DataGridViewTextBoxColumn
    Friend WithEvents rate As DataGridViewTextBoxColumn
    Friend WithEvents rate1 As DataGridViewTextBoxColumn
    Friend WithEvents rate2 As DataGridViewTextBoxColumn
    Friend WithEvents rate3 As DataGridViewTextBoxColumn
    Friend WithEvents rate4 As DataGridViewTextBoxColumn
    Friend WithEvents rate5 As DataGridViewTextBoxColumn
    Friend WithEvents jmlrate As DataGridViewTextBoxColumn
    Friend WithEvents cspetugas As DataGridViewTextBoxColumn
    Friend WithEvents offistirahat As DataGridViewTextBoxColumn
    Friend WithEvents jml1 As DataGridViewTextBoxColumn
    Friend WithEvents waktuistirahat As DataGridViewTextBoxColumn
    Friend WithEvents jml2 As DataGridViewTextBoxColumn
    Friend WithEvents break As DataGridViewTextBoxColumn
    Friend WithEvents jml3 As DataGridViewTextBoxColumn
    Friend WithEvents ibadah As DataGridViewTextBoxColumn
    Friend WithEvents jml4 As DataGridViewTextBoxColumn
    Friend WithEvents cmblevel1 As ComboBox
    Friend WithEvents cmblevel2 As ComboBox
    Friend WithEvents cmblevel3 As ComboBox
    Friend WithEvents cmblevel4 As ComboBox
    Friend WithEvents cmblevel5 As ComboBox
    Friend WithEvents cmb_shift1 As ComboBox
    Friend WithEvents cmb_shift2 As ComboBox
    Friend WithEvents cmb_shift3 As ComboBox
    Friend WithEvents cmb_shift4 As ComboBox
    Friend WithEvents cmb_shift5 As ComboBox
    Friend WithEvents cmb_shift6 As ComboBox
    Friend WithEvents strip_istirahat As ContextMenuStrip
    Friend WithEvents DetailIstirahatToolStripMenuItem As ToolStripMenuItem
End Class
